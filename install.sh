#!/bin/bash

PY_PACKAGES="powerline-status \
             powerline-gitstatus"
PY_VERSION=$(python3 --version | cut -d " " -f 2 | cut -b 1-3)

# todo: Add installation for vs-code, vim
# todo: Update vimrc for powerline and other plugins
# todo: Set up config for vs-code for fonts and extensions

function install_py_packages {
    echo "Installing python packages"
    py_install="python3 -m pip install --user"
    for package in $PY_PACKAGES; do
        $py_install $PY_PACKAGES
    done
    echo "Done"
}

function install_fonts {
    echo "Installing fonts"
    git clone https://github.com/powerline/fonts
    if test "$(uname)" = "Darwin" ; then
        # MacOS
        font_dir="$HOME/Library/Fonts"
    else
        # Linux
        font_dir="$HOME/.local/share/fonts"
        mkdir -p $font_dir
    fi
    echo "Copying Meslo Slashed to $font_dir"
    cp fonts/Meslo\ Slashed/*.ttf $font_dir
    # Reset font cache on Linux
    if which fc-cache >/dev/null 2>&1 ; then
        echo "Resetting font cache, this may take a moment..."
        fc-cache -f "$font_dir"
    fi
    rm -rf fonts
    echo "Done"
}

function add_local_config {
    echo "Adding local configurations"
    echo "Adding configs to bashrc"
    sed "s/PY_VERSION/$PY_VERSION/g" bashrc_append >> $HOME/.bashrc
    echo "Done with bashrc"
    echo "Adding powerline config"
    conf_dir="$HOME/.config"
    mkdir -p $conf_dir
    cp -r powerline $conf_dir
    echo "Done with powerline config"
    echo "Done"
}

install_py_packages
install_fonts
add_local_config
