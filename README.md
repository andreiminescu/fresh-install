# fresh-install

This is a collection of cofigurations for new fresh install for ubuntu

It includes:

- poweline and powerline-gitstatus config
- updated Meslo font for poweline thanks to https://github.com/powerline/fonts
- some aliases for bash
